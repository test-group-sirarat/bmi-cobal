       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. SIRARAT.
       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       
       01  WEIGHT      PIC  999v99 VALUE 0.
       01  HEIGHT      PIC 999v99 VALUE 0.
       01  BMI         PIC 99v99 VALUE 0 .
       01  HEIGHT-POW2 PIC 999v99 VALUE 0 .
     
      
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "BODY MASS INDEX: BMI" 
           DISPLAY "Enter WEIGHT :-" WITH NO ADVANCING
           ACCEPT WEIGHT 

           DISPLAY "Enter HEIGHT:-" WITH NO ADVANCING
           ACCEPT HEIGHT 
           COMPUTE HEIGHT-POW2  = (HEIGHT/100)*(HEIGHT/100)
           COMPUTE BMI =WEIGHT / HEIGHT-POW2  
           IF BMI < 18.5  THEN
                DISPLAY "BMI: UNDERWEIGHT" 
           END-IF
           IF BMI >=18.6 AND  BMI <=24.9 THEN
                DISPLAY "BMI: NORMORE" 
           END-IF
           IF BMI >=25 AND  BMI <=29.9 THEN
                DISPLAY "BMI: OVERWEIGHT" 
           END-IF
           IF BMI >=30 AND  BMI <=34.9 THEN
                DISPLAY "BMI: OBESE" 
           END-IF
           IF BMI >35 THEN
                DISPLAY "BMI: EXTERMLY OBESE" 
           END-IF
           DISPLAY "BMI: " BMI 
           GOBACK

           .